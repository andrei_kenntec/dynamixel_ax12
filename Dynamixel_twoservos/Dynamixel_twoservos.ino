// Test motor joint mode

#include "DynamixelMotor.h"
#include <LiquidCrystal.h>


// software serial pins, if you use software serial
#define SOFT_RX_PIN 3
#define SOFT_TX_PIN 4

const uint8_t M1id=1; // Set up motor IDs here
const uint8_t M2id=2;

// select the pins used on the LCD panel
LiquidCrystal lcd(8, 9, 4, 5, 6, 7);
// define some values used by the panel and buttons
int lcd_key     = 0;
int adc_key_in  = 0;
#define btnRIGHT  0
#define btnUP     1
#define btnDOWN   2
#define btnLEFT   3
#define btnSELECT 4
#define btnNONE   5

int motor1speed;
int motor2speed;
int motor1target;
int motor2target;

// communication baudrate
const long unsigned int baudrate = 9615;

// hardware serial without tristate buffer
// see blink_led example, and adapt to your configuration
SoftwareDynamixelInterface interface(SOFT_RX_PIN, SOFT_TX_PIN);

DynamixelMotor motor1(interface, M1id);
DynamixelMotor motor2(interface, M2id);

uint8_t led_state=true;

int CurrentState = 0;

// read the buttons
int read_LCD_buttons()
{
 adc_key_in = analogRead(0);      // read the value from the sensor 
 // my buttons when read are centered at these valies: 0, 144, 329, 504, 741
 // we add approx 50 to those values and check to see if we are close
 if (adc_key_in > 1000) return btnNONE; // We make this the 1st option for speed reasons since it will be the most likely result
 // For V1.1 us this threshold
 if (adc_key_in < 50)   return btnRIGHT;  
 if (adc_key_in < 250)  return btnUP; 
 if (adc_key_in < 450)  return btnDOWN; 
 if (adc_key_in < 650)  return btnLEFT; 
 if (adc_key_in < 850)  return btnSELECT;  


 return btnNONE;  // when all others fail, return this...
}

void motor1setposition(int position, int speed){
  
  motor1.speed(speed);
  motor1.goalPosition(position);
  motor1target = position;
}

void motor2setposition(int position, int speed){
  
  motor2.speed(speed);
  motor2.goalPosition(position);
  motor2target = position;
}

bool motor1inposition()
{
    if( abs(motor1.currentPosition()-motor1target) <= 5){
    return HIGH;
  }else{
    return LOW;
  }
}

bool motor2inposition()
{
    if( abs(motor2.currentPosition()-motor2target) <= 5){
    return HIGH;
  }else{
    return LOW;
  }
}


void setup()
{ 
  //Setup LCD
  lcd.begin(16, 2);              // start the library
  lcd.setCursor(0,0);

  //setup motors
  interface.begin(baudrate);
  delay(100);

  Serial.begin(9600);
  
  // check if we can communicate with the motor
  // if not, we turn the led on and stop here
  uint8_t status=motor1.init();
  if(status!=DYN_STATUS_OK)
  {
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, LOW);
    while(1);
  }

  uint8_t status1=motor2.init();
  if(status1!=DYN_STATUS_OK)
  {
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, LOW);
    while(1);
  }
 

  motor1.enableTorque();  
  motor2.enableTorque();  

  // set to joint mode, with a 180° angle range
  // see robotis doc to compute angle values
  motor1.jointMode(204, 820);
 
  motor2.jointMode(204, 820);

}

void loop() 
{

  switch(CurrentState) {
/////////////////////////
case 0:
  //motor 1 go to middle position
  lcd.setCursor(0,0);
  lcd.print("M1 mid sp 300");

  motor1setposition(512, 300);
  
  if( motor1inposition() ){
  CurrentState = 1;
  }
 break;

case 1:
  //motor 1  move 45° CCW
  lcd.setCursor(0,0);
  lcd.print("M1 45CCW sp 500");
  
  motor1setposition(666, 500);
  
  if( motor1inposition() ){
  CurrentState = 2;
  }
break; 

case 2:
  //motor 1  go to middle position
  lcd.setCursor(0,0);
  lcd.print("M1  mid sp 600");
  
  motor1setposition(512, 600);
  
  if( motor1inposition() ){
  CurrentState = 3;
  }
break;

case 3:
  //motor 1  move 45° CW
  lcd.setCursor(0,0);
  lcd.print("M1 45CW sp 300");

  motor1setposition(358, 300);
  
  if( motor1inposition() ){
  CurrentState = 4;
  }
break;

case 4:
  //motor 1 LED toggle
  lcd.setCursor(0,0);
  lcd.print("M1 LED Flash!!");
  
 motor1.write(DYN_ADDRESS_LED, led_state);
  led_state=!led_state;
  CurrentState = 5;
break;

case 5:
  //motor 2 go to middle position
  lcd.setCursor(0,0);
  lcd.print("M2 mid sp 300");
  
  motor2setposition(512, 300);
  
  if( motor2inposition() ){
  CurrentState = 6;
  }
 break;

case 6:
  //motor 2  move 45° CCW
  lcd.setCursor(0,0);
  lcd.print("M2 45CCW sp 500");
  
  motor2setposition(666, 500);
  
  if( motor2inposition() ){
  CurrentState = 7;
  }
break; 

case 7:
  //motor 2  go to middle position
   lcd.setCursor(0,0);
  lcd.print("M2  mid sp 600");
  
  motor2setposition(512, 600);
  if( motor2inposition() ){
  CurrentState = 8;
  }
break;

case 8:
  //motor 2  move 45° CW
   lcd.setCursor(0,0);
  lcd.print("M2 45CW sp 300");
  
  motor2setposition(358, 300);
  
  if( motor2inposition() ){
  CurrentState = 9;
  }
break;

case 9:
  //motor 2 LED toggle
  lcd.setCursor(0,0);
  lcd.print("M1 LED Flash!!");
  
 motor2.write(DYN_ADDRESS_LED, led_state);
  led_state=!led_state;
  CurrentState = 0;
break;
  
  }


 
}

